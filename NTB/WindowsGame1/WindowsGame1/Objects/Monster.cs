﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace WindowsGame1
{
    class Monster:Player
    {
        public Monster(Game1 game, string name, string pathTxt)
            : base(game,name,pathTxt)
        {
            content = game.Content;
        }   
    }
}
