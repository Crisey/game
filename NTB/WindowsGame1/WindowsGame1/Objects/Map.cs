﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1.Game
{
    static class Map
    {
        static private short _x;

        static public short x
        {
            get { return _x; }
            set { _x = value; }
        }

        static private short _y;

        static public short y
        {
            get { return _y; }
            set { _y = value; }
        }

        static private short _z;

        static public short z
        {
            get { return _z; }
            set { _z = value; }
        }
    
    }
}
