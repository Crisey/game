﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1
{
    class Drawing : DrawableGameComponent
    {
        public ContentManager content;
        public Vector2 Position;
        public Color Color;
        public string Text;
        public int WhatMin;
        public int WhatMax;

        private SpriteBatch spriteBatch;
        private SpriteFont spriteFont;

        public Drawing(Game1 game, Vector2 position, Color color, string text)
            : base(game)
        {
            content = game.Content;
            Position = position;
            Color = color;
            Text = text;
        }

        public Drawing(Game1 game, Vector2 position, Color color, string text, int whatMin)
            : base(game)
        {
            content = game.Content;
            Position = position;
            Color = color;
            Text = text;
            WhatMin = whatMin;
        }

        public Drawing(Game1 game, Vector2 position, Color color, string text, int whatMin, int whatMax)
            : base(game)
        {
            content = game.Content;
            Position = position;
            Color = color;
            Text = text;
            WhatMin = whatMin;
            WhatMax = whatMax;
        }

         protected override void LoadContent()
         {
             spriteBatch = new SpriteBatch(GraphicsDevice);
             spriteFont = content.Load<SpriteFont>("assets/Fonts/Info");
         }

         protected override void UnloadContent()
         {
             content.Unload();
         }

         public override void Draw(GameTime gameTime)
         {
             string newText = string.Empty;

             if (WhatMax != 0)
                 newText = Text + ": " + WhatMin + "/" + WhatMax;
             else if (WhatMin != 0)
                 newText = Text + ": " + WhatMin;
             else if (Text != string.Empty)
                 newText = Text;
             
             spriteBatch.Begin();
             spriteBatch.DrawString(spriteFont, newText, Position, Color);
             spriteBatch.End();
         }
    }
}
