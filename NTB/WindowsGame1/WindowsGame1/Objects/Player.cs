﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    class Player : DrawableGameComponent
    {
        public ContentManager content;
        public SpriteBatch spriteBatch;
        Camera2d camera = new Camera2d();
        
        public Player(Game1 game, byte classes, string nickname, string pathTxt)
            : base(game)
        {
            content = game.Content;
        }

        public Player(Game1 game, string name, string pathTxt)
            : base(game)
        {
            content = game.Content;
        }

        #region config
        private byte _characterClass;

        public byte characterClass
        {
            get { return _characterClass; }
            set { _characterClass = value; }
        }
        
        private string _nick;

        public string nick
        {
            get { return _nick; }
            set { _nick = value; }
        }

        private Texture2D _txt;

        public Texture2D txt
        {
            get { return _txt; }
            set { _txt = value; }
        }

        private Color _color;

        public Color color
        {
            get { return _color; }
            set { _color = value; }
        }

        private float _rotation;

        public float rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }
        #endregion config
        #region position
        private ushort _x;

        public ushort x
        {
            get { return _x; }
            set { _x = value; }
        }

        private ushort _y;

        public ushort y
        {
            get { return _y; }
            set { _y = value; }
        }

        private ushort _z;

        public ushort z
        {
            get { return _z; }
            set { _z = value; }
        }
        #endregion position
        #region attributies
        
        private int _hpPerLvl;

        public int hpPerLvl
        {
            get { return _hpPerLvl; }
            set { _hpPerLvl = value; }
        }
        
        private int _startMana;

        public int startMana
        {
            get { return _startMana; }
            set { _startMana = value; }
        }

        private int _manaPerLvl;

        public int manaPerLvl
        {
            get { return _manaPerLvl; }
            set { _manaPerLvl = value; }
        }

        private int _startHp;

        public int startHp
        {
            get { return _startHp; }
            set { _startHp = value; }
        }

        private int _level;

        public int level
        {
            get { return _level; }
            set { _level = value; }
        }
        
        private int _hp;

        public int hp
        {
            get { return _hp; }
            set { _hp = value; }
        }

        private int _mana;

        public int mana
        {
            get { return _mana; }
            set { _mana = value; }
        }
        private int _speed;

        public int speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        private int _maxHp = 135;

        public int maxHp
        {
            get { return _maxHp; }
            set { _maxHp = value; }
        }

        private int _maxMana = 35;

        public int maxMana
        {
            get { return _maxMana; }
            set { _maxMana = value; }
        }
        private int _maxSpeed = 5;

        public int maxSpeed
        {
            get { return _maxSpeed; }
            set { _maxSpeed = value; }
        }
        #endregion attributies

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            
            _maxHp = _startHp + (_level * _hpPerLvl);
            _maxMana = _startMana + (_level * _manaPerLvl);

            KeyboardState keyboard = Keyboard.GetState();
        }

        public override void Draw(GameTime gameTime)
        {

            spriteBatch.Begin(SpriteSortMode.BackToFront,
            BlendState.AlphaBlend,
            null,
            null,
            null,
            null,
            camera.get_transformation(GraphicsDevice));
            spriteBatch.Draw(_txt, new Rectangle(_x * 32, _y * 32, 32, 32), Color.White);
            spriteBatch.End();
        }
    }
}
