using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace WindowsGame1
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        FrameRateCounter fpsCounter;
        Drawing[] draw = new Drawing[5];
        Player player;
        Camera2d cam = new Camera2d();

        static Monster[] monster = new Monster[5];
        static NPC[] npc = new NPC[5];
        
        SpriteFont info;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreparingDeviceSettings += PrepareDevice;
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 650;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {   
            fpsCounter = new FrameRateCounter(this, new Vector2(25, 15), Color.Green, Color.Black);

            fpsCounter.Initialize();

            Game.Map.x = 2500;
            Game.Map.y = 2000;
            Game.Map.z = 14;

            Thread init = new Thread(setValue);
            init.Start(); Thread.Sleep(30);

            base.Initialize();

            IsMouseVisible = true;
            IsFixedTimeStep = true;
            graphics.SynchronizeWithVerticalRetrace = false;
            Window.AllowUserResizing = true;
            graphics.ApplyChanges();
        }
        protected override void LoadContent()
        {
            player = new Player(this, Character.classes = 3, "Crisey", "assets/Imgs/monsters/player");
            player.Initialize();
            draw[0] = new Drawing(this, new Vector2(30, 125), Color.White, "Hp", player.hp, player.maxHp);
            draw[1] = new Drawing(this, new Vector2(30, 145), Color.Blue, "Mana");
            draw[2] = new Drawing(this, new Vector2(30, 165), Color.Yellow, "Speed");
            draw[3] = new Drawing(this, new Vector2(30, 185), Color.Aquamarine, "x");
            draw[4] = new Drawing(this, new Vector2(30, 205), Color.Aquamarine, "y");
            for (int i = 0; i < draw.Length; i++)
                draw[i].Initialize();

            player.hp = 10;

            spriteBatch = new SpriteBatch(GraphicsDevice);
            player.txt = Content.Load<Texture2D>("assets/imgs/monsters/player");
            info = Content.Load<SpriteFont>("assets/Fonts/info");
        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            base.Update(gameTime);

            fpsCounter.Update(gameTime);
            player.Update(gameTime);
            cam.Pos = new Vector2(player.x * 32, player.y * 32);
        }

        protected override void Draw(GameTime gameTime)
        {

            spriteBatch.Begin();
            
            spriteBatch.Draw(player.txt, new Rectangle(150, 150, 200, 200), Color.Red);
            
            spriteBatch.End();
            
            for (int i = 0; i < draw.Length; i++)
                draw[i].Draw(gameTime);
            
            fpsCounter.Draw(gameTime);
        }

        public static void setValue()
        {
            for (int i = 0; i < monster.Length; i++)
            {
                //monster[i] = new Monster();
            }
            for (int i = 0; i < npc.Length; i++)
            {
                //npc[i] = new NPC(this,"l","assets/Imgs/monsters/monster");
            }
        }

        protected void PrepareDevice(object sender, PreparingDeviceSettingsEventArgs e)
        {
            e.GraphicsDeviceInformation.PresentationParameters.RenderTargetUsage = RenderTargetUsage.PreserveContents;
        }
    }
}
